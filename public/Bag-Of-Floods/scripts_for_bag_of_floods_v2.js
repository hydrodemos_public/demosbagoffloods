function display_about() {
  alert("what can a bag full of marbles tell us about flood hazard ?");
}

function RAZ_compteur() {
  n_total = 0;
  n_noires = 0;
  n_vertes = 0;
  n_bleues = 0;
  n_rouges = 0;
  tb_nb_red.textContent = " - ";
  tb_nb_blue.textContent = " - ";
  tb_nb_green.textContent = " - ";
  tb_nb_black.textContent = " - ";
  tb_freq_red.textContent = " theoretical = 1/100";
  tb_freq_blue.textContent = " theoretical = 1/100";
  tb_freq_green.textContent = " theoretical = 8/100";
  tb_freq_black.textContent = " theoretical = 90/100";
  clear_token_history("stats and tokens cleared");
  q_highest = 0;
  T_highest = 0;
  text_proba.innerHTML = "  ";

}

function clear_token_history(message) {
  x = x0 ;
  y = y0 ;
  coord_noires = raz_xy ;
  coord_vertes = raz_xy ;
  coord_bleues = raz_xy ;
  coord_rouges = raz_xy ;
  //sur_ligne_rouge = 1 ;
  //sur_ligne_noire = 1 ;
  //sur_ligne_verte = 1 ;
  //sur_ligne_bleue = 1 ;

  text_result.innerHTML = message ; // "No tokens yet" or "Old tokens cleared ! " ;  // A DECOMMENTER APRES DEFINITION ALIAS
  tokens_black.setAttribute("d", coord_noires);
  tokens_green.setAttribute("d", coord_vertes);
  tokens_blue.setAttribute("d", coord_bleues);
  tokens_red.setAttribute("d", coord_rouges);
  svg_object_last_drawn_marble.style.fill= "grey";
}

function mise_a_jour_affichages() {
  text_proba.innerHTML = "probability = " + last_freq.toFixed(6) + " ;  T = " +  (1/(1-last_freq)).toFixed(0)  + " yrs" ;
  tokens_green.setAttribute("d", coord_vertes);
  tokens_blue.setAttribute("d", coord_bleues);
  tokens_red.setAttribute("d", coord_rouges);
  tokens_black.setAttribute("d", coord_noires);

  if (last_color== "black") {
    last_text = " under 0.90 : black marble ! " ; // + coord_noires ;
    last_text_color = "#000";
	last_result = "this year, the highest flood has a discharge with a return period lesser than 10 years" ;
  }
  else if (last_color== "blue") {
      last_text = " between 0.98 and 0.99 : blue marble !" ;
      last_text_color = "#00d" ;
	  last_result = "this year, the highest flood has a discharge with a return period between 50 and 100 years" ;
 }
 else if (last_color== "green") {
    last_text = "  between 0.90 and 0.98 : green marble !"  ;
    last_text_color = "#0d0" ;
	last_result = "this year, the highest flood has a discharge with a return period between 10 and 50 years" ;
	}
 else {
     last_text = " above 0.99 : red marble !" ;
     last_text_color = "#d00";
     last_result = "this year, the highest flood has a discharge with a return period superior to 100 years ";
 };
  text_calcul.innerHTML = last_text ; // + coord_noires ;
  text_calcul.style.color = last_text_color ;
  svg_object_last_drawn_marble.style.fill= last_color;
  text_result.innerHTML = last_result ;

  tb_nb_red.textContent = n_rouges;
  tb_nb_blue.textContent = n_bleues;
  tb_nb_green.textContent = n_vertes;
  tb_nb_black.textContent = n_noires;
  tb_freq_red.textContent = (n_rouges/n_total).toFixed(3);
  tb_freq_blue.textContent = (n_bleues/n_total).toFixed(3);
  tb_freq_green.textContent = (n_vertes/n_total).toFixed(3);
  tb_freq_black.textContent = (n_noires/n_total).toFixed(3);
}

// FONCTIONS tirage = juste un "token" de couleur
function un_tirage() {

  randomColor_Ajoutaserie();
  text_cumul.innerHTML = "After one more draw, " + n_total + " marble(s), with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";
  mise_a_jour_affichages();

}

function randomColor_Ajoutaserie() {

  let freq =  Math.random() ;
  let new_coord = " " + x + " " + y  ;
  last_freq = freq ;

  if (freq < 0.9) {
    last_color= "black";
	n_noires ++;
	//if (!sur_ligne_noire){
	//    sur_ligne_noire +=1 ;
	//    coord_noires+=" M "}
	coord_noires+=new_coord ;

  }
  else if (freq < 0.98) {
    last_color= "green";
	n_vertes ++;
	//if (!sur_ligne_verte){
	//    sur_ligne_verte +=1 ;
	//    coord_vertes+=" M "}
	coord_vertes+=new_coord ;
  }
  else if (freq < 0.99) {
     last_color= "blue";
	 n_bleues ++;
	 //if (!sur_ligne_bleue){
	 //   sur_ligne_bleue +=1 ;
	 //   coord_bleues+=" M "}
	 coord_bleues+=new_coord ;
  }
  else {
     last_color= "red";
	 n_rouges ++ ;
	 //if (!sur_ligne_rouge){
	 //   sur_ligne_rouge +=1 ;
	 //   coord_rouges+=" M "}
	 coord_rouges+=new_coord ;
	 };

  n_total ++ ;

  x += deltax ;
  if (x > largeur_maxi){
  x = x0 ;
  y = y + deltax;
  //sur_ligne_rouge = 0;
  //sur_ligne_bleue = 0;
  //sur_ligne_verte = 0;
  //sur_ligne_noire = 0;

  };
}

function randomColor_Serie_i_j(maxi, maxj) {

  let nb_billes_serie = maxi*maxj ;
  let freq ;
  let new_coord ;

  // past history is not cleared (will be for the specific call of a Series of 100 )
  text_calcul.innerHTML = " Draw a series of " + nb_billes_serie + " marbles" ;

  for (let i=0; i<maxi;i++){
  for (let j=0; j<maxj;j++){

  freq =  Math.random() ;
  new_coord = " " + x + " " + y  ;

 if (freq < 0.9) {
	n_noires ++;
	coord_noires+=new_coord ;
	last_color= "black";
  }
  else if (freq < 0.98) {
 	n_vertes ++;
	coord_vertes+=new_coord ;
	last_color= "green";
  }
  else if (freq < 0.99) {
    n_bleues ++;
	coord_bleues+=new_coord ;
	last_color= "blue";
  }
  else {
    n_rouges ++ ;
	coord_rouges+=new_coord ;
	last_color= "red";
	};

  n_total ++ ;
  x += deltax ;
   }
  y += deltax;
  x = x0;
  }

  last_freq = freq ;
  mise_a_jour_affichages();

  text_cumul.innerHTML = "SERIES of " + n_total + " marbles, with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";

}
function cent_tirages() {
  text_calcul.innerHTML = " Clear history and draw a series of 10 x 10 marbles" ; // overload the Series function message
  clear_token_history("Old tokens cleared before a series of 100 x 100 ! ");
  RAZ_compteur();

  randomColor_Serie_i_j(10,10);
  text_cumul.innerHTML = "SERIES of 100 marbles, with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";

}

function n_tirages(number) {
  // nombre_pour_serie = field_num.value; // seulement si validé par ENTER
  btn_drawN.value = "Draw "+ nombre_pour_serie + " more marbles" ;
  text_calcul.innerHTML = " Add a series of "+ nombre_pour_serie + " marbles" ; // overload the Series function message

  for (let i=0; i<nombre_pour_serie;i++){
  randomColor_Ajoutaserie();
  }
  mise_a_jour_affichages();
  text_cumul.innerHTML = "After "+nombre_pour_serie+ " more marbres, the total is" + n_total + " marbles, with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";

}
// FONCTIONS EVENEMENT

function Q_de_f(f){
  // temporarily : exponential distribution for quick trials ;
   // not very correct : at least for didactic purposes, to be replaced by a Gumbel distribution, better suited for "one draw = max flood of the year"
   return q0 + pente * Math.log(1/(1-f));
}


function mise_a_jour_affichages_events() {
  //text_proba.innerHTML = "Highest probability = " +  last_freq.toFixed(4)  + " T = " +  (1/(1-last_freq)).toFixed(0)  + " yrs ; q(T)= " + (q0 + pente  * Math.log(1/(1-last_freq)) ) + " ; q1 = " + q1.toFixed(3) + " ; q2 = " + q2.toFixed(3);
  //text_proba.innerHTML = "Highest recorded flood = " + (50 * q_highest.toFixed(2)) + " ; estimated T = " +  T_highest.toFixed(0)   + " yrs ";
  text_proba.innerHTML = "Highest recorded flood peak : estimated T = " +  T_highest.toFixed(0)   + " yrs ";

  tokens_green.setAttribute("d", coord_vertes);
  tokens_blue.setAttribute("d", coord_bleues);
  tokens_red.setAttribute("d", coord_rouges);
  tokens_black.setAttribute("d", coord_noires);

  if (last_color== "black") {
    last_text = " under 0.90 : black marble (peak descriptor) ! " ; // + coord_noires ;
    last_text_color = "#000";
	last_result = "this year, the highest flood has a peak discharge with a return period lesser than 10 years" ;
  }
  else if (last_color== "blue") {
      last_text = " between 0.98 and 0.99 : blue marble (peak descriptor) !" ;
      last_text_color = "#00d" ;
	  last_result = "this year, the highest flood has a peak discharge with a return period between 50 and 100 years" ;
 }
 else if (last_color== "green") {
    last_text = "  between 0.90 and 0.98 : green marble (peak descriptor) !"  ;
    last_text_color = "#0d0" ;
	last_result = "this year, the highest flood has a peak discharge with a return period between 10 and 50 years" ;
	}
 else {
     last_text = " above 0.99 : red marble ! (peak descriptor)" ;
     last_text_color = "#d00";
     last_result = "this year, the highest flood has a peak discharge with a return period superior to 100 years ";
 };
  text_calcul.innerHTML = last_text ; // + coord_noires ;
  text_calcul.style.color = last_text_color ;
  svg_object_last_drawn_marble.style.fill= last_color;
  text_result.innerHTML = last_result ;

  tb_nb_red.textContent = n_rouges;
  tb_nb_blue.textContent = n_bleues;
  tb_nb_green.textContent = n_vertes;
  tb_nb_black.textContent = n_noires;
  tb_freq_red.textContent = (n_rouges/n_total).toFixed(3);
  tb_freq_blue.textContent = (n_bleues/n_total).toFixed(3);
  tb_freq_green.textContent = (n_vertes/n_total).toFixed(3);
  tb_freq_black.textContent = (n_noires/n_total).toFixed(3);
}

function un_event() {
  randomEvent_Ajoutaserie();
  text_cumul.innerHTML = "After one more event, " + n_total + " marbles, with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";
  mise_a_jour_affichages_events()
}

function serie_events(number) {

  // nombre_pour_serie = field_num.value; // seulement si validé par ENTER;
  text_calcul.innerHTML = " Add a series of "+ nombre_pour_serie + " pseudo-events" ; // overload the Series function message

  for (let i=0; i<nombre_pour_serie;i++){
  randomEvent_Ajoutaserie();
  }
  mise_a_jour_affichages_events();
  text_cumul.innerHTML = "After "+nombre_pour_serie+ " more pseudoevents,, the total is" + n_total + " marbles, with : " +  n_rouges + " red, "+ n_bleues  + " blue and " + n_vertes + " green";
}


function randomEvent_Ajoutaserie() {
  // to draw illustrative flood event-looking tokens, we fixed arbitrarily 5 points, with two peaks separated by a central point
  // by construction, the central point is a fraction of the first peak, and can be higher "than the second peak"
  let freq =  Math.random() ;
  let duree = 5 + Math.floor(10 * Math.random());  // duration was initially set arbitrarily as proportionnal to frequency
  let coef_second_pic = Math.random(); // ratio to deduce the lowest peak value from the drawn peak value
  let ordre_pics = Math.random(); // in half of the cases, the highest peak comes first, otherwise it comes second
  let redescente =  Math.random(); // set the value of the central point, defined as a ratio multiplied by the first peak value
  var q_premier_pic = 0 ;
  var q_second_pic = 0 ;

  let max_q1_q2 = Q_de_f(freq) ;

  if (max_q1_q2 > q_highest) {  q_highest =  max_q1_q2;  T_highest = 1/(1-freq);}

  if (ordre_pics > 0.5){
     q_premier_pic =  max_q1_q2 ;
     q_second_pic = coef_second_pic * coef_second_pic * max_q1_q2  ;
  }
  else {
     q_second_pic =  max_q1_q2 ;
     q_premier_pic = coef_second_pic * coef_second_pic * max_q1_q2 ;
  }

  let new_coord = " M " + x + " " + y  ;

  if (freq < 0.9) {
    last_color= "black";
	n_noires ++;
	coord_noires+= new_coord ;
	coord_noires+= " " + (x+5) + " " + (y - q_premier_pic) ;
	coord_noires+= " " + (x+8) + " " + (y - q_premier_pic * redescente) ;
	coord_noires+= " " + (x+8  + 0.5*duree) + " " + (y - q_second_pic) ;
    coord_noires+= " " + (x+11 + duree) + " " + y ;
  }
  else if (freq < 0.98) {
    last_color= "green";
	n_vertes ++;
	coord_vertes+=new_coord ;
	coord_vertes+= " " + (x+5) + " " + (y - q_premier_pic) ;
	coord_vertes+= " " + (x+10) + " " + (y - q_premier_pic * redescente);
    coord_vertes+= " " + (x+11 + duree/2) + " " + (y - q_second_pic) ;
    coord_vertes+= " " + (x+11 + duree) + " " + y ;
  }
  else if (freq < 0.99) {
     last_color= "blue";
	 n_bleues ++;
	 coord_bleues+=new_coord ;
	 coord_bleues+= " " + (x+5) + " " + (y - q_premier_pic) ;
	 coord_bleues+= " " + (x+10) + " " + (y - q_premier_pic * redescente) ;
     coord_bleues+= " " + (x+11 + duree/2) + " " + (y - q_second_pic) ;
     coord_bleues+= " " + (x+11 + duree) + " " + y ;
  }
  else {
     last_color= "red";
	 n_rouges ++ ;
	 coord_rouges+=new_coord ;
	 coord_rouges+= " " + (x+5) + " " + (y - q_premier_pic) ; ;
	 coord_rouges+= " " + (x+10) + " " + (y - q_premier_pic * redescente) ;
     coord_rouges+= " " + (x+11 + duree/2) + " " + (y - q_second_pic) ;
     coord_rouges+= " " + (x+11 + duree) + " " + y;
	 };

  n_total ++ ;

  x += deltax ;
  if (x > largeur_maxi){
  x = x0 ;
  y = y + deltax;

  };
  last_freq = freq ;
  q1 = q_premier_pic;
  q2 = q_second_pic;

}


// CONTROLE TYPE DE FONCTIONS SELON CHOIX D'AFFICHAGE
function modif_tokens(){
var radioBtn_tokens_chk = document.querySelector('input[name="option_historique"]:checked').value;
// alert("changing to "+radioBtn_tokens_chk )
if (radioBtn_tokens_chk == "circles"){
    alert("tokens are now circles, but they are not the marbles themselves");
    if (current_token_option == "hyd"){
     window.location.reload() ;
     btn_draw1.value = "DRAW ONE MARBLE";
     btn_draw1.style.color = "black"};
    tokens_black.setAttribute('class', "ClassBlackCircles"); //  "url(#marker1355_bille_noire)" ;
    tokens_green.setAttribute('class', "ClassGreenCircles"); //  "url(#marker1172_bille_verte)" ;
    tokens_blue.setAttribute('class', "ClassBlueCircles");  //  "url(#marker1193_bille_bleue)" ;
    tokens_red.setAttribute('class',"ClassRedCircles"); // "url(#marker1259_bille_rouge)"
    raz_xy = "M "+ (x0 - deltax) + " " + y0 ;
    current_token_option = "circles";


}
else if (radioBtn_tokens_chk == "ticks"){
    alert("tokens are now ticks, one per past marble (same colour)")
    if (current_token_option == "hyd"){
     window.location.reload();
     btn_draw1.value = "DRAW ONE MARBLE";
     btn_draw1.style.color = "black"};
    tokens_black.setAttribute('class',"ClassBlackTicks"); // "url(#marker1355_tick_noir)" ;
    tokens_green.setAttribute('class',"ClassGreenTicks"); // "url(#marker1172_tick_vert)" ;
    tokens_blue.setAttribute('class', "ClassBlueTicks"); // "url(#marker1193_tick_bleu)" ;
    tokens_red.setAttribute('class', "ClassRedTicks"); // "url(#marker1259_tick_rouge)";
    raz_xy = "M "+ (x0 - deltax) + " " + y0 ;
    current_token_option = "ticks";
}
else if (radioBtn_tokens_chk == "hyd"){
    alert("tokens now begin to look like flood events (but only the peak is taken into account for the estimation of a return period ?)")
    clear_token_history()
    x = x0  ;
    y =  y0  ;
    raz_xy = "" ;
    coord_noires = raz_xy ;
    coord_vertes = raz_xy ;
    coord_bleues = raz_xy ;
    coord_rouges = raz_xy ;
    tokens_black.setAttribute('class',"ClassNoMarkers"); // "url(#marker1355_tick_noir)" ;
    tokens_green.setAttribute('class',"ClassNoMarkers"); // "url(#marker1172_tick_vert)" ;
    tokens_blue.setAttribute('class', "ClassNoMarkers"); // "url(#marker1193_tick_bleu)" ;
    tokens_red.setAttribute('class', "ClassNoMarkers"); //
    tokens_black.style.fill="black"; //
    tokens_green.style.fill="green"; //
    tokens_blue.style.fill= "blue"; //
    tokens_red.style.fill= "red"; //
    svg_object_bag.removeEventListener("click", un_tirage);
    btn_draw1.removeEventListener("click", un_tirage);
    btn_draw100.removeEventListener("click", cent_tirages);
    btn_drawN.removeEventListener("click", n_tirages);
    svg_object_bag.addEventListener("click", un_event);
    btn_draw1.addEventListener("click", un_event);
    btn_draw1.value = "DRAW ONE pseudoEVENT";
    btn_draw1.style.color = "blue";
    btn_draw100.disabled = true ;
    btn_drawN.addEventListener("click", serie_events);
    btn_drawN.value = "Draw "+ nombre_pour_serie + " more pseudoEVENTs";
    btn_drawN.style.color = "blue"
    current_token_option = "hyd"

}
}
//INITIALISATIONS

// compteur de billes tirées
var n_total = 0;
var n_noires = 0;
var n_vertes = 0;
var n_bleues = 0;
var n_rouges = 0;

// coordonnées des tokens et pas d'espace
const y0 = 30 ;
const x0 = 20 ;
const deltax = 30 ;
var raz_xy = "M "+ (x0 - deltax) + " " + y0 ;
var coord_noires = raz_xy ;
var coord_vertes = raz_xy ;
var coord_bleues = raz_xy ;
var coord_rouges = raz_xy ;
//var sur_ligne_rouge = 1 ;
//var sur_ligne_noire = 1 ;
//var sur_ligne_verte = 1 ;
//var sur_ligne_bleue = 1 ;

var x = x0 ;
var y = y0 ;
const largeur_maxi = 640 - deltax ;
const nombre_pour_serie_default = 10 ;

var nombre_pour_serie = 10;

var last_color = "grey";
var last_text = "no marble drawn yet";
var last_text_color = "grey";
var last_result = "no marble drawn yet";

// loi expo Qpointe(T) = q0 + pente * T
const q0 = 5 ;
const pente = 2.5 ;
var q1 ;
var q2 ;
var q_highest=0 ;
var T_highest=0 ;

// ALIASES

var btn_test_a_propos = document.getElementById("btn_apropos");

var btn_composition = document.getElementById("to_composition");
var collaps_composition = document.getElementById("composition");

// svg drawing : last drawn marble
var svg_object_big_marble = document.getElementById("path2502_bille_rouge_devant_sac");
var svg_object_last_drawn_marble = document.getElementById("circle_marble");
// svg drawing : bag
var svg_object_bag = document.getElementById("g3896_group_bag");

// svg drawing : history (tokens)
var btn_clear_tokens =  document.getElementById("clear_tokens");
var tokens_black =  document.getElementById("path_billes_noires") ;
var tokens_green =  document.getElementById("path_billes_vertes") ;
var tokens_blue =  document.getElementById("path_billes_bleues") ;
var tokens_red =  document.getElementById("path_billes_rouges") ;

// right pannel (actions)
const btn_draw1 =  document.getElementById("Draw_1");
const btn_draw100 =  document.getElementById("Draw_100");
const btn_drawN = document.getElementById("Draw_N");
const field_num = document.getElementById("num_field");
const btn_tokens = document.getElementById("send_radiobutton");

var current_token_option = document.querySelector('input[name="option_historique"]:checked').value;


// left pannel (output)

var text_result = document.getElementById("resultat");
var text_proba = document.getElementById("proba");
var text_calcul = document.getElementById("calcul");
var text_cumul = document.getElementById("cumul");
var text_highest = document.getElementById("highest");

var tb_nb_red = document.getElementById("nb_red");
var tb_nb_blue = document.getElementById("nb_blue");
var tb_nb_green = document.getElementById("nb_green");
var tb_nb_black = document.getElementById("nb_black");
var tb_freq_red = document.getElementById("freq_red");
var tb_freq_blue = document.getElementById("freq_blue");
var tb_freq_green = document.getElementById("freq_green");
var tb_freq_black = document.getElementById("freq_black");

var btn_clear_stats =document.getElementById("clear_stats");

// INITs
field_num.value = nombre_pour_serie ;   // A DECOMMENTER APRES DEFINITION ALIAS

//svg_object_big_marble.style.fill= "grey";
svg_object_last_drawn_marble.style.fill= "grey";
collaps_composition.style.maxHeight = collaps_composition.scrollHeight + "px";

//LISTENERS

btn_test_a_propos.addEventListener("click", display_about);
svg_object_bag.addEventListener("click", un_tirage);
btn_draw1.addEventListener("click", un_tirage);
btn_draw100.addEventListener("click", cent_tirages);
btn_drawN.addEventListener("click", n_tirages);
btn_tokens.addEventListener("click", modif_tokens);

btn_clear_tokens.addEventListener("click", function(){clear_token_history("tokens cleared");});
btn_clear_stats.addEventListener("click", RAZ_compteur);

btn_composition.addEventListener("click", function() {

    if (collaps_composition.hidden){
      collaps_composition.hidden = false ; // collaps_composition.scrollHeight + "px";
      collaps_composition.setAttribute("font-size", "12")
      btn_composition.value = "hide explanation about random draw";
    }
     else {
       collaps_composition.hidden = true;
       collaps_composition.setAttribute("font-size", "1")
      btn_composition.value = "want to know how it works ?";    }
    } );

field_num.addEventListener("keypress", function(event) {
  // If the user presses the "Enter" key on the keyboard
  if (event.key === "Enter") {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the actions
    btn_drawN.value = "Draw "+field_num.value + " more marbles";
    nombre_pour_serie = field_num.value;
  }
});

// Main


clear_token_history('No tokens yet');




