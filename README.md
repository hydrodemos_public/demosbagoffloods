# Bag of floods    WORK (apps and doc) IN PROGRESS...

What can marbles in a bag tell us about flood hazard ? Is drawing the only red marble out of 100 analoguous to experiencing a 100-yr flood ? Almost, but not quite... Then, once we know what a return period means, how can we guess the probabilility to be flooded on a duration of several years ? Let's draw series of marbles - successively. How does the outcome vary from one series to another ? Let's talk about variability, using interactive tools made for longs series of draws and their analysis. 

### Outline : interactive tools (here) and doc (not too far)

Due to technical issues, the project is currently split with interactive HTLM apps running on [these pages](https://hydrodemos_public.pages.mia.inra.fr/demosbagoffloods/), while information is on a [wiki](https://gitlab.irstea.fr/hydrotools_demosandprocessing/bag-of-floods/-/wikis/home) and 

The "bag of flood" project is a spin-off of a more advanced didactic project in french, "sac de crue", hosted  [in gitlab EauDyssee project page ](https://gitlab.irstea.fr/christine.poulard/eaudyssee-codesdecalcul/activity) .
"Sac de crue" currently offers several apps in html and Python, detailed in wiki pages for professionals, and is presented for the general public on the website of the association EauDyssée. 
More detailed explanations will be gradually added to the wiki pages of this english version gitlab.
 

### More details

The first version in English (and later with several languages ?) is a clone of the "sac de crue" html counterpart in French". An improved version is now available, and still in developpement. Its added features are :
- a better layout, with all buttons placed on the right ;
- stats of past draws in a table (number and empirical frequencies of each colour).
- several ways of representing the tokens, including 'pseudo-events', with variants in shape. This is an invitation to discuss the difference between coloured marbles and hydrographs: colors represent the return period of the peak discharge, but a flood event has other characteristics, like udration and thus volume.

Similar codes in Python, with more features and explanatory texts, exist, [so far in French only](https://gitlab.irstea.fr/christine.poulard/eaudyssee-codesdecalcul).


Description of the general project « bag of floods » :

Probabilistic flood hazard assessment gives invaluable insights to decision-makers as such and also as a first step to estimate risk indicators, like Annual Average Damages. The results of flood hazard probabilistic assessement can be published for the general public, usually in the form of maps. Although probabilistic concepts, like the « 100-year flood », are commonly used, for instance in newspapers, flood hazard assessment is more complex than it seems and interpretation is tricky.

This demo is a tool designed to help discussing this concept. To make flood quantiles more concrete, we suggest using marbles which colour corresponds to a class of return period. Discussing the analogies and differences between drawing a marble from the bag and the next annual flood make it easier to explain that flood hazard assessment  (i) must not be focussed on the « 100-yr flood », (ii) is often expressed as a probability over one given year, but for planning it should be estimated over a much longer duration (like successive draws from the bag) and (iii) variability is significant and matters. 
Scripts allowing to simulate long series of draws confirm that empirical probabilities get close to theoretical probabilities, but also illustrate less intuitive results : on average one quarter of 100 successive draws, contains two floods or more with a discharge exceeding the « 100-yr discharge». 




